[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Account - current condition

### Portfolio

At any one time, there can be only one current portfolio in an account, which is characterized as follows:

* `total_rpl` - realized profit in the account currency 
	- `rpl` - accumulated realized profit
	- `maker_fee` - maker fee
	- `taker_fee` - taker fee
	- `swap` - accumulated swap difference account currency/dollar
	- `refinance` - refinancing for perpetual contracts
* `total_upl` - unrealized profit in dollars
	- `upl` - unrealized profit on positions
	- `swap_upl` - accumulated swap difference account currency/dollar
* `cross_margin` - cross margining:
	- `init_cross_margin` - initial cross margin
	- `main_cross_margin` - support cross margin
* `solid_balance` - hard account balance - the sum of external balances and balances of closed portfolios
* `account_index_score` - account currency rate against the dollar
* `leverage` - average leverage of all bundles

Realized profit for a portfolio is the cumulative value of the financial result of closed positions from the moment it was opened. Unrealized profit is an assessment of the financial result at the current mark price.

All realized profits: accumulated realized profits, commissions, swaps and refinancing, are calculated in the account currency, and unrealized profits and collateral are always calculated in the main currency (USD).

Upon opening (appearance of the first open position) and closing, the state of the portfolio is logged.

Learn more about [realized/unrealized profits](exec.md) and
[funds](available-funds.md#balance-and-available-funds)


### Bundle

A bundle is a combination of all positions into one cross pair, on the basis of which the risks are assessed and a decision on liquidation is made.

* `total_rpl` - realized profit in the account currency
	- `rpl` - accumulated realized profit
	- `maker_fee` - maker fee
	- `taker_fee` - taker fee
	- `swap` - accumulated swap difference account currency/dollar
	- `refinance` - refinancing for perpetual contracts
* `total_upl` - unrealized profit in dollars 
	- `upl` - unrealized profit by position
	- `swap_upl` - accumulated swap difference account currency/dollar
* `cross_margin` - cross margining:
	- `init_cross_margin` - initial cross margin 
	- `main_cross_margin` - support cross margin 
* `margin` - margin:
	- `init_margin` - initial margin, excluding coverage 
	- `main_margin` - support margin, excluding coverage 
* `account_index_score` - the account currency rate against the dollar 
* `main_cover_long_usd` - coverage for long positions 
* `main_cover_short_usd` - coverage for short positions 
* `leverage` - the  bundle leverage 
* `limit_min_price` - lower spot liquidation price 
* `limit_max_price` - upper spot liquidation price 
* `delta` - delta and other Greeks 

Risk assessment, in the form of cross-collateral, is conducted taking into account the coverage of all positions, and as a result, the leverage ratio and liquidation levels are calculated.

For example: 5 bought March futures and 5 sold June futures per one cross pair,  minimally dependent on the movement of the spot, since the prices of marking for both contracts directly depend on the weighted index of the spot price, and this is also taken into account in the prices of liquidation.

Learn more about [bundle cross-margin estimates](margin.md#bundle), [Greek](greek.md#greeks),
[liquidation levels](liquidation.md#evaluation-of-liquidation-levels) and [leverage](liquidation.md#bundle-leverage-assessment)


### Position

A position is the condition associated with only one specific contract, which reflects all the data for accounting for financial results and collateral:

* `total_rpl` - realized profit in the account currency
	- `rpl` - accumulated realized profit
	- `maker_fee` - maker fee
	- `taker_fee` - taker fee
	- `swap` - accumulated swap difference account currency/dollar
	- `refinance` - refinancing for perpetual contracts
* `total_upl` - unrealized profit in dollars
	- `upl_first` - unrealized profit
	- `upl_second` - unrealized profit on the second cross-pair (for a composite contract)
	- `swap_upl` - accumulated swap difference account currency/dollar
* `margin` - margin:
	- `init_margin` - initial margin, excluding coverage
	- `main_margin` - support margin, excluding coverage
* `buying_quantity` - amount to buy  
* `selling_quantity` - quantity to sell
* `long_quantity` - long position (`short_quantity` == 0)
* `short_quantity` - short position (`long_quantity` == 0)
* `account_index_score` - account currency rate against the dollar
* `main_cover_long_usd` - coverage for long positions 
* `main_cover_short_usd` - cover for short positions 
* `delta` - delta and other Greeks

### Portfolio financial result

The financial result of the portfolio, expressed in `total_rpl`, at the time of closing will be reflected in the trade balance of the sub-account, and participates in the formation of the hard balance of the new portfolio.

More about [the balance of the trading subaccount.](account.md#hard-balance-of-a-trading-sub-account)
