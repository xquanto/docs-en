[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Transactions

### Trading commission

The maker is the party to the trade that executes the order after it has been registered in the order book and provides liquidity to the market. A taker is a party that executes a transaction without registering an order and moves the market. Different commissions are assigned for the maker and taker.
Upon completion of the transaction, the commission is separately taken into account in the account currency, at the current rate:

```math
F = F^{maker} + F^{taker}
```

$`F`$ - commission for the transaction  
$`F^{maker}`$ - maker fee for a transaction  
$`F^{taker}`$ - taker fee for a transaction  

[Contract fees](#to-do-contract-fee)


### Increasing of a position

A completed transaction may lead to an increase in the number of contracts in a position.

Such a trade will change the average entry price and average entry time. Also, for a composite contract, the average entry price of the second price will change, for which the fair price index for the contract from the second cross-pair will be taken as the current second price.

Average entry price is calculated as follows:

```math
p_a = \frac{q_p p'_a + q_e p_e} {q_p + q_e}
```

$`p_a`$ - average entry price   
$`p'_a`$ - average entry price, before the deal  
$`p_e`$ - the price at which the deal was made  
$`q_p`$ - the number of contracts in the position, before the deal   
$`q_e`$ - number of contracts in a trade  

Average entry time for any position will be needed later when fixing:

```math
t_a = \frac{q_p t'_a + q_e t_e} {q_p + q_e}
```

$`t_a`$ - average entry time  
$`t'_a`$ - average entry time, before the deal  
$`t_e`$ - time at which the transaction was made  

*For composite futures, the average price is calculated separately for the long and short components.*

[Composite cross pair](#cross-composite)


### Fixing a position

Profit fixation is a transaction resulting in a decrease in the number of contracts in a position.

[Futures price function](greek.md#greek-future) for calculating financial result:

```math
R = 100 q_e \frac{p_e - p_a}{p_e}
```

[Option price function](greek.md#greek-option) for calculating financial result:

```math
R = q_e (p_e - p_a)
```

$`R`$ - realized profit on a trade   
$`p_a`$ - average entry price  
$`p_e`$ - the price at which the deal was made  
$`q_e`$ - number of contracts in a trade  

For both cases, the profit is received in dollars, which means it must be converted into the account currency, and take into account the swap of providing the dollar for the transaction, against the account currency, at current rates, for the entire period of use of funds.

Using the [adjustment factor](margin.md#position) to evaluate the collateral, the amount in dollars of the swap can be estimated:

```math
u_{leg} = 100 k^f q_e \max(1, \mathrm{delta})
```

$`u_{leg}`$ - amount in dollar base (usd leg)  
$`k^f`$ - correction factor for quantum futures  
$`q_e`$ - number of contracts in a trade  
$`delta`$ - contract delta  


The swap itself:

```math
S = u_{leg} (r_c - r_u) (t - t_a)
```

$`S`$ - implemented swap on trade  
$`r_c`$ - current overnight rate for the account currency  
$`r_u`$ - current overnight rate per dollar  
$`t`$ - current time of the deal  
$`t_a`$ - average time to enter a position  

The resulting swap result must also be converted into the account currency.

*For composite futures, calculations are carried out separately for the long and short components.*

Read more about [adjustment factor](margin.md#position)

### Splitting a trade when closing a position

If a trade is received that closes a position and opens an opposing one, such a deal is artificially split into two parts: the first, that closes the position, and the second marked with a flag `is_cheap`.


### Transaction types

Full list of all flagged trade types:

* `is_reduced` - flag that position is reduced
* `is_filled` - flag that the order is closed
* `is_opened` - flag that a position is open
* `is_closed` - flag that the position is closed
* `is_bundle_opened` - flag that the bundle is open
* `is_bundle_closed` - flag that the bundle is closed
* `is_portfolio_opened` - flag that the portfolio is open
* `is_portfolio_closed` - flag that the portfolio is closed 
* `is_cheap` - flag that a fragment remains after closing
* `is_supply` - flag that is involved in the delivery
* `is_settlement` - flag that is involved in the calculation
* `is_liquidation` - flag of liquidation
* `is_deleverage` - flag of deliverage


### Examples of calculating profit and loss for a transaction

#### Inverse Quanto-Futures on Main Cross Pair

Examples (calculation of profit or loss):

**Example 1:**  
20 contracts sold at 15,000, bought at 12,000

```math
-20 \sdot 100 \sdot (12000 - 15000) / 12000
```

Financial result is 500 USD.

**Example 2:**  
20 contracts bought at 15,000, sold at 20,000:

```math
20 \sdot 100 \sdot (20000 - 15000) / 20000
```

Financial result is 500 USD.

*Attention! When the price moves down, the profit or loss will be greater than if the market moves by the same difference!*

**Example 3.1:**  
20 contracts bought at 12000, sold at 15000:

```math
20 \sdot 100 \sdot (15000 - 12000) / 15000
```

 Financial result is 400 USD.

**Example 3.2:**  
30 contracts bought at 20000, sold at 15000:

```math
30 \sdot 100 \sdot (15000 - 20000) / 15000
```

Financial result is -1000 USD.

**Example 3.3:**  
20 contracts bought at 12000, and then 30 more contracts bought at 20000, then all sold at 15000. Let’s find the average price after purchases:

```math
(20 \sdot 12000 + 30 \sdot 20000) / (20 + 30)
```
This corresponds to an  average price of 16800, and similarly we will calculate the financial result:

```math
50 \sdot 100 \sdot (15000 - 16800) / 15000
```

The Financial result is -600 USD, as if the position were split into separate trades, Examples 3.1 and 3.2


#### Inverse quanto composite cross pair futures

Examples (calculation of profit or loss for LTCBTC-ZZ):

**Example 1.1:**  
30 contracts were bought at a price of 0.005 with the value of the BTCUSD-ZZ.F index 12000 (60), sold at a price of 0.006 with the value of the BTCUSD-ZZ.F index 12000 (72). Let's break the composite contract into two components, a long position for the first currency against the LTCUSD dollar: buy 30@60 and sell 30@72. And the short position of the second currency against the dollar BTCUSD: sell 30@12000 and buy 30@12000.   

Under the first contract in a composite cross pair (long):


```math
30 \sdot 100 \sdot (72 - 60) / 72
```

Financial result is 500 USD

Under the second contract (short):

```math
-30 \sdot 100 \sdot (12000 - 12000) / 12000
```

Financial result is 0 USD

Total financial result is 500 USD


**Example 1.2:**  
30 contracts bought at 0.005 when the index is BTCUSD-ZZ.F 12000 (60), sold at 0.006 when the index is BTCUSD-ZZ.F 10000 (60).

Under the first contract in a composite cross pair (long):


```math
30 \sdot 100 \sdot (60 - 60) / 60
```

Financial result is 0 USD

Under the second contract (short):

```math
-30 \sdot 100 \sdot (10000 - 12000) / 10000
```

Financial result is 600 USD

Total financial result is 600 USD

**Example 1.3:**  
30 contracts bought at 0.005 when the index is BTCUSD-ZZ.F 12000 (60), sold at 0.006 when the index is BTCUSD-ZZ.F 15000 (90).

Under the first contract in a composite cross pair (long):

```math
30 \sdot 100 \sdot (90 - 60) / 90
```

Financial result is 1000 USD

Under the second contract (short):

```math
-30 \sdot 100 \sdot (15000 - 12000) / 15000
```

Financial result is -600 USD

Total financial result is 400 USD

**Example 2:**  
30 contracts bought at 0.005 when the index is BTCUSD-ZZ.F 12000 (60), sold at 0.005 when the index is BTCUSD-ZZ.F 15000(75).

Under the first contract in a composite cross pair (long):

```math
30 \sdot 100 \sdot (75 - 60) / 75
```

Financial result is 600 USD

Under the second contract (short):

```math
-30 \sdot 100 \sdot (15000 - 12000) / 15000
```

Financial result is -600 USD

Total financial result is 0 USD

*Attention! The financial result does not change at a constant composite price, regardless of the behavior of the second cross pair.*


#### Vanilla options contract on inverse quanto-futures on the main cross pair

**Example 1:**  
20 bought at 60, sold at 80:

```math
20 \sdot (80 - 60)
```

Financial result is 400 USD.

