[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Cross Pairs

### The Main Cross Pairs

The Main Cross Pairs are the pairs where the local currency is USD:

* `BTCUSD`
* `ETHUSD`
* `LTCUSD`
* `XRPUSD`
* `EURUSD`
* `USDTUSD`

A spot index for the general cross pairs is calculated using a weighted method.

[Methodology for calculation of the weighted index for the main cross pairs.](index.md#weighted-index-for-the-general-cross-pair-i)


### Bonded (composite) cross pairs

The bonded cross pairs are modeled from the main cross pairs: 

* `ETHBTC` - long `ETHUSD` and short `BTCUSD`
* `LTCBTC` - long `LTCUSD` and short `BTCUSD`
* `XRPBTC` - long `XRPUSD` and short `BTCUSD`
* `LTCETH` - long `LTCUSD` and short `ETHUSD`
* `XRPETH` - long `XRPUSD` and short `ETHUSD`
* `XRPLTC` - long `XRPUSD` and short `LTCUSD`

There are also two components in the financial result for such a cross pair.

Read more about [the calculation of a composite cross pair profit](exec.md#fixing-a-position)
and [the examples of composite cross pairs](exec.md#inverse-quanto-composite-cross-pair-futures).

[A weighted index of the bonded cross pairs](index.md#weighted-index-for-the-composite-cross-pair-i) is calculated for the composite cross pairs.