[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Perpetual contract specification (ETHBTC-ZZ)

|Name											|Value								|
|-------------------------------------------------------|---------------------------------------|
|Contract 												|ETHBTC-ZZ								|
|Type 													|Perpetual futures contract 		|
|Expiration date										|Perpetual								|
|Contract marking index							|ETHBTC-ZZ.M (`ETHBTC-ZZ.M`) 			|
|Fair contract price index						|ETHBTC-ZZ.F (`ETHBTC-ZZ.F`) 			|
|Premium contract index								|ETHBTC-ZZ.P (`ETHBTC-ZZ.P`%AYR) 		|
|Contract open interest index					|ETHBTC-ZZ.O (`ETHBTC-ZZ.O`) 			|
|Underlying asset											|ETHBTC 								|
|Underlying asset type									|composite 							|
|Additional index in the P&L calculation 					|BTCUSD-ZZ.F (`BTCUSD-ZZ.F`) 			|
|Weighted index on the underlying asset						|ETHBTC.I (`ETHBTC.I`) 					|
|Historical volatility index for the underlying asset 	|ETHBTC.H (`ETHBTC.H`) 					|
|Base currency of the underlying asset							|ETH 									|
|Base currency overnight index							|ETH.N (`ETH.N`%AYR) 					|
|Base currency forward index						|ETH-ZZ.W (`ETH-ZZ.W`%AYR) 				|
|Second currency of the underlying asset							|BTC 									|
|Overnight second currency index							|BTC.N (`BTC.N`%AYR) 					|
|Second currency forward index						|BTC-ZZ.W (`BTC-ZZ.W`%AYR) 				|
|Refinancing contract index						|ETHBTC-ZZ.R (`ETHBTC-ZZ.R`%AYR) 		|
|Refinancing period of the contract						|every 8 hours						 	|
|Refinancing rate of the contract in the current period	|`fixed ETHBTC-ZZ.R`				 	|
|Time until the end of the current funding period	|`front TODO` 							|
|Settlement currency										|USD 									|
|Contract size										|100 USD 								|
|Initial											|1.2 USD 								|
|Supportive										|1.0 USD 								|
|Execution of calculations									|no 							|
|Minimum price change								|`0.0000025`	 						|
|Autodelivery                                         |yes                                   |
|Taker fee                                      |`0.1` USD                              |
|Maker fee                                       |`0.0` USD                              |
|Liquidation fee                                    |`0.5` USD                              |
|Frozen trades                                       |no                                    |
|Maximum application									|1000 contracts 						|
|Base position limit									|10000 contracts 						|

## Term contract specification (ETHBTC-U1)

|Name											|Value								|
|-------------------------------------------------------|---------------------------------------|
|Contract 												|ETHBTC-U1								|
|Type 													|Term futures contract 			|
|Expiration date										|`17.09.2021`							|
|Time to expiration									|`front TODO` 							|
|Expiry price										|`expiration_price` 					|
|Second expiry price									|`expiration_price_second` 				|
|Time to expiration									|`front TODO` 							|
|Contract Marking Index							|ETHBTC-U1.M (`ETHBTC-U1.M`) 			|
|Fair Contract Price Index						|ETHBTC-U1.F (`ETHBTC-U1.F`) 			|
|Premium contract index								|ETHBTC-U1.P (`ETHBTC-U1.P`%AYR) 		|
|Contract open interest index					|ETHBTC-U1.O (`ETHBTC-U1.O`) 			|
|Underlying asset											|ETHBTC 								|
|Underlying asset type									|composite 							|
|Additional index in the P&L calculation					|BTCUSD-U1.F (`BTCUSD-U1.F`) 			|
|Weighted index on the underlying asset						|ETHBTC.I (`ETHBTC.I`) 					|
|Historical volatility index for the underlying asse 	|ETHBTC.H (`ETHBTC.H`) 					|
|Base currency of the underlying asset							|ETH 									|
|Base currency overnight index						|ETH.N (`ETH.N`%AYR) 					|
|Base Currency Forward Index						|ETH-U1.W (`ETH-U1.W`%AYR) 				|
|Second currency of the underlying asset							|BTC 									|
|Overnight second currency index							|BTC.N (`BTC.N`%AYR) 					|
|Second currency forward index						|BTC-U1.W (`BTC-U1.W`%AYR) 				|
|Settlement currency										|USD 									|
|Contract size										|100 USD 								|
|Initial GO										|1.2 USD 								|
|Supportive GO										|1.0 USD 								|
|Execution of calculations									|no 							|
|Calculation index                                        |ETHBTC.I                               |
|Additional calculation index                          |BTCUSD.I                               |
|Minimum price change								|`0.0000025`	 						|
|Autodelivery                                         |yes                                   |
|Taker Fee                                       |`0.1` USD                              |
|Maker fee                                       |`0.0` USD                              |
|Liquidation Fee                                    |`0.5` USD                              |
|Frozen trades                                       |no                                    |
|Maximum application									|1000 contracts 						|
|Base position limit									|10000 contracts 						|

TODO: The average expected volatility index must be calculated for all options


----------------------------------------------------------------------

Index symbol for currency pair: BTCUSD.I 
Marking index symbol: BTCUSD-ZZ.M 
Refinancing rate index symbol: BTCUSD-ZZ.R 
Refinancing interval: every 8 hours.
Time to refinancing: 5 hours. 
Auto-deleveraging enabled: Yes. 
Risk limit: 200 000. 
Limit step: 50 000 
Execution of calculations: Not performed. 
Min. price change: 0.5 
Max. price: 16 383 
Max. amount: 536 870 911 
Taker fee: 0.075% 
Maker fee: -0.025%

March 2019 BTCUSD Futures Contract:
Main Ticker: BTCUSD-H9 
Expiration Date: 03/15/2019 T23: 59: 59.99999Z00 
Settlement Currency: USD 
Exchange Currency: BTC 
Contract Size: 1 USD 
Initial GO: 1.00% + Taker Entry Fee + Exit Fee taker Support GO: 0.50% + taker exit fee + funding rate 
Index symbol for a currency pair: BTCUSD.I 
Marking index symbol: BTCUSD-H9.M 
Auto-deleveraging enabled: Yes 
Risk limit: 200,000 
Limit step: 50,000 
Settlement execution: Not are produced. 
Min. price change: 0.5 
Max. price: 1,000,000 Taker fee: 0.075% Maker fee: -0.025%

March 2019 option contract call 4500 on BTCUSD: 
Main ticker: BTCUSD-4500С9 
Expiry date: 03/14/2019 
Strike: 4500 
Option type: Call 
Underlying asset: BTCUSD-H9 
Settlement currency of the underlying asset: USD 
Exchange currency of the underlying asset: BTC 
Size of the underlying asset contract : 1 USD 
Seller's starting price: TODO Support. 
Seller's HO: TODO 
Auto-deleveraging enabled: Yes 
Risk limit: TODO 
Limit step: TODO 
Settlement execution: Deliverable 
Min. price change: 5 
Max. price: 1,000,000 
Taker fee: 0.25% 
Maker fee: -0.05%

LTCBTC perpetual composite futures contract: 
Main ticker: LTCBTC-ZZ 
Expiry date: Perpetual 
First contract: LTCUSD-ZZ 
Second contract: BTCUSD-ZZ 
Settlement currency: USD 
Exchange currency of the first contract: LTC 
Exchange currency of the second contract: BTC 
Contract size: 1 USD 
Initial GO: 2.50% + taker entry fee + taker exit fee Support. 
GO: 1.25% + taker exit fee + funding rate 
Marking index symbol for the second contract: BTCUSD-ZZ.M 
Currency pair index symbol: LTCBTC.I 
Marking index symbol: LTCBTC-ZZ.M 
Refinancing rate index symbol: LTCBTC-ZZ .R 
Refinancing interval: every 8 hours 
Time to refinancing: 5 hours 
Auto deleveraging enabled: Yes 
Marking method: FairPrice 
Risk limit: 100,000 
Limit step: 25,000 
Settlement execution: Long position on the first contract and short on the second. 
Min. price change: 0.001 
Max. price: 1,000,000 
Taker fee: 0.25% 
Maker fee:-0.05%

Ticker Root XBTUSD Expiry Date Perpetual, subject to Early Settlement Initial Margin 1.00% + Entry Taker Fee + Exit Taker Fee Maint. Margin 0.35% + Exit Taker Fee + Funding Rate Interest Base Symbol .XBTBON8H Interest Quote Symbol .USDBON8H Funding Premium Symbol .XBTUSDPI8H Funding Rate -0.0566% Funding Interval every 8 hours Next Funding Jun 21, 2021, 11:00:00 PM Predicted Rate -0.0237% Mark Price 32629.66 Auto-Deleveraging Enabled Yes: This contract is highly speculative and manages loss through auto-deleveraging. Mark Method FairPrice Fair Basis -10.16 Fair Basis Rate -61% Fair Basis Calculation The fair basis on this instrument is determined by an annualized calculation of the funding rate. Risk Limit 200 000 mXBT Risk Step 100 000 mXBT Open Interest 507 214 200 24H Turnover 68 227 692,784 mXBT Total Volume 3 129 052 078 789 Type Settled in XBT, quoted in USD Contract Size 1 USD (Currently 0,03065 mXBT per contract) Settlement This contract is perpetual and does not settle. Subject to Early Settlement. Fee See the Fees Reference for more details. Minimum Price Increment 0.5 USD Max Price 1 000 000 Max Order Quantity 10 000 000 Lot Size 100
