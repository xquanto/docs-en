[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Expiration and refinancing

### Delivery of options

All options expire on the last nanosecond of Thursday (year, month, and week depends on specification). The underlying asset for an option is an inverse quanto futures. At the time of delivery, the mark price for the futures is fixed, and if at this price the option is classified as "in the money", then the option is automatically delivered at the strike price between the holder and the seller (writer) in the form of a transaction, for a call option:
* Call option holder: Selling the option at a price of 0
* Seller of a call option: buying an option at a price of 0
* Call option holder: Buy (long) a futures at the strike price
* Seller of a call option: Sell (short) the futures at the strike price

For put option:
* Put option holder: selling the option at a price of 0
* Put option seller: buying an option at a price of 0
* Put option holder: Sell (short) the futures at the strike price
* Put option seller: buying (long) a futures at the strike price

All deals are flagged `is_supply`

If the option is not "in the money" then delivery does not take place, and such a position is mutually debited from the accounts of the holder and the seller.


### Futures calculation

All futures expire in the last nanosecond of the third Friday of the quarter month. Upon expiration, the calculation takes place in relation to the underlying asset - the spot price. At the moment of calculation, the value of the weighted index is fixed, and the calculation is performed as the financial result of the position with the average entry price, and the closing price - the fixed spot price, in the form of a transaction:

* Futures buyer: Selling a futures contract at a fixed spot price
* Futures seller: Buying a futures contract at a fixed spot price

All deals are flagged `is_settlement`


### Refinancing perpetual contracts

For perpetual contracts, every 8 hour period is refinanced at the end of the period. The rate determined by the refinancing index is fixed at the beginning of this period and will be applied at the end. The refinancing value that the position holder will receive:

```math
R^{pos} = \frac{1} {3 \cdot 365} \bar{q}r_r
```

$`R^{pos}`$ - is the amount of refinancing of the position
$`\bar{q}`$ - the number of perpetual contracts, taking into account the direction of the position
$`r_r`$ - the refinancing rate

<!-- To realization: ~~Такие сделки отмечены флагом `is_settlement`~~ -->
