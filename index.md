[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Indices

### Currency Indices

#### Overnight currency rate .N

Each currency is characterized by its own short-term overnight interest rate, which for fiat currencies is set by the central bank, and for cryptocurrencies is determined by forecasting short-term rates using the [BDT mode](http://pluto.mscc.huji.ac.il/~mswiener/research/Benninga73.pdf)
The input data are the dynamics of forward rates for the currency and different dates.

```math
I_n = F_{bdt}(I_w^*)
```

$`I_n`$ - overnight rate index  
$`I_w^*`$ - all forward indices for one currency and their dynamics
  

*The index is used as a rate for calculating the marking index for perpetual contracts.*

*To stabilize the result, there is a limit on the change in the amount of 1 percentage point in an 8-hour interval.*

Under development


### Cross pair indices

#### Weighted index for the general cross pair. I

The weighted index for the main cross-pair is calculated as the average of the fair prices of the cross-pair on other sites (exchanges). The fair price is modeled as the average price between a model buy trade of USD 10,000 and a model sell trade of USD 10,000. For each cross-pair, a list of sites is defined, for which the average value is taken.

BTCUSD:
* BTCUSD.E.BTMP – external index BTCUSD, exchange Bitstamp
* BTCUSD.E.KRKN - external index BTCUSD, exchange Kraken
* BTCUSD.E.GDAX - external index BTCUSD, exchange Coinbase
* BTCUSD.E.GMIN - external index BTCUSD, exchange Gemini


ETHUSD:
* ETHUSD.E.BTMP - external index ETHUSD, exchange Bitstamp
* ETHUSD.E.KRKN - external index ETHUSD, exchange Kraken
* ETHUSD.E.GDAX - external index ETHUSD, exchange Coinbase
* ETHUSD.E.BTFIN - external index ETHUSD, exchange Bitfinex

LTCUSD:
* LTCUSD.E.BTMP - external index LTCUSD, exchange Bitstamp
* LTCUSD.E.GDAX - external index LTCUSD, exchange Coinbase
* LTCUSD.E.BTFIN - external index LTCUSD, exchange Bitfinex
* LTCUSD.E.GMIN - external index LTCUSD, exchange Gemini


XRPUSD:
* XRPUSD.E.BTMP - external index XRPUSD, exchange Bitstamp
* XRPUSD.E.KRKN - external index XRPUSD, exchange Kraken
* XRPUSD.E.BTFIN - external index XRPUSD, exchange Bitfinex
* XRPUSD.E.LQUID - external index XRPUSD, exchange Liquid


USDTUSD:
* USDTUSD.E.KRKN - external index USDTUSD, exchange Kraken
* USDTUSD.E.BTFIN - external index USDTUSD, exchange Bitfinex


```math
I_i = I_{ex_1} + I_{ex_2}
```

$`I_i`$ - weighted index of the general cross pair    
$`I_{ex_i}`$ - external fair price indices for the general cross pair

*The index plays the role of a spot, and is used as the value of the underlying asset value in calculating the labeling and Greek index, calculating the expected volatility for options, the main criterion in relation to liquidation prices, and as the settlement value at futures expiration.
Weighted index for the composite cross pair. I*

#### Weighted index for the composite cross pair. I

Weighted index **.I** A composite cross-pair is calculated as the ratio of weighted indices: the first currency to the dollar and the second currency to the dollar. 

```math
I_i^{cur_i/cur_j} = \frac{I_i^{cur_i/usd}} {I_i^{cur_j/usd}}
```

$`I_i^{cur_i/cur_j}`$ - weighted index of the composite cross pair  
$`I_i^{cur_i/usd}`$ - weighted indices for the main cross pairs  

*The index plays the role of a spot, and is used as the value of the underlying asset value in calculating the labeling and Greek index, calculating the expected volatility for options, the main criterion in relation to liquidation prices, and as the settlement value at futures expiration.*

[Composite cross pair](#cross-composite)


#### Historical Volatility Index.H

To model other indices, the historical volatility value for the cross pair may be needed. The index value is calculated as the standard deviation of historical data on the daily timeframe:

```math
E = \frac{1}{30} \sum_{i < 30} {\ln(c_i / c_{i+1})}
```

```math
I_h = \sqrt{\frac{365}{30}} \sum_{i < 30} \left( \ln(c_i / c_{i+1}) - E \right)^2
```

$`I_h`$ - historical volatility index for the cross pair

*The index is more informative in nature, and is used as initial values (default, if there is no way to get the exact value), for the index of expected volatility for a futures contract.*

*To stabilize the result, there is a limit on the change in the amount of 20 percentage points in an 8-hour interval.*


### Contract indices

#### Trading index (last trade index) of the contract

The trading index is indicated without a suffix and matches the contract symbol. The index is formed from the price of the last deal within the exchange tick. Together with the trading index for the specified time-frames, the volume value is calculated, which is formed as the cumulative number of contracts in transactions for a certain time-frame interval.

*The index is not used for calculations, but plays an important role in decision-making for clients.*


#### Open Interest Index. O

The open interest index is calculated as the cumulative value of changes in open interest in each exchange tick, and reflects the total number of all long contracts on all accounts (the number of short contracts must have the symmetric property)

*The index is strictly informative*


#### Premium rate index.P

Индекс ставка-премиум выступает в качестве стабилизатора для расчёта индекса справедливой цены.

The premium rate index acts as a stabilizer for calculating the fair price index.
The index is based on the base fair price, which is defined as the average price between a model buy trade equivalent to USD 10,000 and a model sell trade equal to USD 10,000:

```math
I_f^{base} = \frac {A_b + A_s} {2}
```

$`I_f^{base}`$ - the fair price base value  
$`A_b`$ - Average price for a \$10,000 purchase   
$`A_s`$ - Average price for a \$10,000 sale  

This value is fixed, in the form of a rate, in relation to the marking index:

```math
I_p = \frac{1} {t} \cdot \ln \frac{I_f^{base}} {I_m}
```

$`I_p`$ - premium rate index    
$`t`$ - time to futures expiration, at least 8 hours  

*To stabilize the result, there is a limit on the change in the amount of 8 percentage points in an 8-hour interval.*

*The index is used to calculate the fair price index and the refinancing index*


#### Fair price index.F

The fair price index is calculated through the stabilizing premium rate index from the labeling index:

```math
I_f = F^{fut}(I_m,I_p,t)
```

$`I_f`$ - fair price index   
$`t`$ - time to futures expiration, at least 8 hours  

*The index is used to calculate forward rates, estimate the expected volatility of an option, and as the second price for a trade in a composite contract.*

#### Labeling index for futures. M

The labeling index for the futures is calculated as follows:

```math
I_m = F^{fut}(s,r,t)
```

$`I_m`$ - marking index per contract   
$`F^{fut}(s,r,t)`$ - function of dependence of the futures cost on the underlying asset   
$`s`$ - the value of the weighted index for the cross-pair $`I_i`$  
$`r`$ - difference of forward rates for currencies $`I^{cur_2}_w-I^{cur_1}_w`$  
$`t`$ - time to expiration of the futures  

*The index is used to calculate unrealized profits, assess collateral, and to calculate the index of the expected volatility of a futures contract.*

[Dependence of the value of a futures on the underlying asset](greek.md#greek-future)


#### Perpetual contract marking index.M

The marking index for a perpetual contract is calculated as follows:

```math
I_m = F^{fut}(s,r,t)
```

$`I_m`$ - marking index per contract  
$`F^{fut}(s,r,t)`$ - function of dependence of the futures cost on the underlying asset  
$`s`$ - the value of the weighted index for the cross pair $`I_i`$  
$`r_z`$ - difference in overnight rates for currencies $`I^{cur_2}_n-I^{cur_1}_n`$  
$`t_z`$ - time to the end of the 8-hour interval
  

*The index is used to calculate unrealized profits, and to calculate the premium index.*

[Dependence of the value of a futures on the underlying asset](greek.md#greek-future)


#### Option labeling index.M

Option contract marking index is calculated as follows:

```math
I_m = F^{option}(s,k,v,t,u)
```

$`I_m`$ - marking index per contract  
$`F^{option}(s,k,v,t,u)`$ - function of dependence of the futures cost on the underlying asset  
$`s`$ - the value of the weighted index for the cross pair $`I_i`$  
$`k`$ - option strike  
$`r`$ - difference in forward rates on currencies $`I^{cur_2}_w-I^{cur_1}_w`$  
$`v`$ - expected volatility per futures contract $`I_u`$  
$`u`$ - time to expiration of an option  
$`t`$ - time to expiration of a futures  

*The index is used to calculate unrealized profits, assess collateral, and to calculate an index of the expected volatility of an option.*

[Dependence of the option value on the underlying asset](greek.md#greek-option)


#### Option implied volatility index.V

The index of the implied volatility of an option contract is calculated as a solution to the inverse problem:

```math
I_f = F^{option}(s,k,v,t,u)
```

$`I_f`$ - fair price index for a contract  
$`F^{option}(s,k,v,t,u)`$ - a function of the dependence of the value of a put or call option on the underlying asset  
$`s`$ - the value of the weighted index for the cross pair $`I_i`$  
$`k`$ - option strike  
$`r`$ - difference in forward rates on currencies $`I^{cur_2}_w-I^{cur_2}_w`$  
$`v`$ - implied volatility per futures contract $`I_v`$   
$`u`$ - time to expiration of an option  
$`t`$ - time to expiration of a futures  

*The index is used to calculate the implied volatility of a futures contract.*

*To stabilize the result, there is a limit on the change in the amount of 10 percentage points in an 8-hour interval.*


#### Implied volatility index for futures.U

The index of implied volatility of a futures contract is calculated as a weighted value of the implied volatility for options:

```math
I_u = \sum_{tk} I_v(t,k)w(t,k)
```

$`I_u`$ - index of implied volatility for futures  
$`I_v(t,k)`$ - index of implied volatility for an option with expiration $`t`$ and strike $`k`$  
$`w(t,k)`$ - the weighting coefficient in which options near money have the greatest weight and a shorter expiration period  

*The index is used to calculate the option marking index*

*To stabilize the result, there is a limitation on the change in the amount of 10 percentage points in an 8-hour interval.*


#### Perpetual contract refinancing index.R

The perpetual contract refinancing index is calculated as the difference in overnight rates:

```math
I_r = I^{cur_2}_n-I^{cur_1}_n
```

$`I_r`$ - refinancing index, as an annualized interest rate, for an 8-hour interval  
$`I^{cur}_n`$ - overnight rate on currency  

*The index is used to carry out refinancing at a given rate*


#### Forward Indices

A forward index is characterized by the date on which the rate is applied and the currency itself. The search for rates is carried out simultaneously for all currencies on one date, using the least squares method of the difference between the fair price indices and the futures estimate, which is used to calculate the marking index. Thus, the optimization problem consists of the objective function:

```math
W = \sum_{ij} \left(I_f^{cur_i/cur_j} - F^{fut}(I_i^{cur_i/cur_j}, r_{cur_j} - r_{cur_i}, t) \right)^2 \rarr \min
```

and restrictions:
```math
\begin{aligned}
r_{usd} = I_n \\
r_{cur} > -0.02
\end{aligned}
```

$`W`$ - Quadratic Objective Optimization Function  
$`I_f^{cur_i/cur_j}`$ - index of the fair price of the contract  
$`F^{fut}`$ - function of dependence of the futures cost on the underlying asset  
$`I_i^{cur_i/cur_j}`$ - cross pair weighted index  
$`r_{cur}`$ - forward rates on the currency, as the required parameter $`I_w^{cur}`$  
$`t`$ - ime to expiration, the same for all contracts  
$`I_n`$ - fiat currency overnight rate index  

*The index is used to calculate the index of marking futures and options, the overnight index and the index of expected volatility for options*

*To stabilize the result, there is a limit on the change in the amount of 1 percentage point in an 8-hour interval.*

### Stabilizing Index Values

For intermediate indices, if indicated in the description, the stabilizing function is used:

```math
\mathrm{clamp}(x, y, d) = \begin{cases}
x + \min(d, |y-x|), &y \geqslant x \\
x - \min(d, |y-x|), &y < x
\end{cases}
```

$`\mathrm{clamp}(x, y, d)`$ - stabilizing function  




