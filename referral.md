[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Referral program

### Referrer bonuses

By inviting a referee, the referrer can receive two types of bonus dependent on actions undertaken by the referee: the completion of 3 steps listed below, and trading.

Bonus for 3 step completion, paid once, in the amount of 20 USD, for each referral, upon completion of the last of the 3 steps (by the referee):
* Registration
* Pass KYC
* One-time deposit of more than 100 USD

The trading bonus is accrued daily, from the taker's commission for all referrals in the amount of 15%


## Referral bonuses

It is also beneficial for the referee to be invited: upon passing the KYC (), the referral is provided with a grace period of up to 3 months, in which the taker's commission is 0.


### Accrual of bonuses

Referral bonuses are credited in  US dollars $, and are credited to the appropriate wallet, and can be immediately used as desired by the client.

[Client wallets ](wallet.md#currency-account-wallet)


### Duration of the referral program

The bonus for 3 step completion  is valid for 1 year, from the moment of registration of the referee (it is necessary that the 3 steps are completed within 365 days).

The trading bonus is valid for 5 years, from the moment of registration of the referee.

The bonus for the grace period is available for 1 year from the date of registration.
