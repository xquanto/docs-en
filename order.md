[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Orders

### Limit Orders

A limit order is accepted with the following parameters: quantity, direction, price and execution instructions. Such an order can be executed at a fixed price or better, or can be registered in the order book.

[Limit Orders](#https://www.sec.gov/fast-answers/answerslimithtm.html)


### Post Limit orders

A post limit order can be sent with execution instructions, and differs from a limit order in that it cannot be executed before it is registered. It can be registered only at the declared price, or it will be rejected, which guarantees execution as a maker.


### Market Orders

A market order has only direction and quantity. Within one tick for market and cross limit orders, the market order has price priority, since it has the best price, and will be executed in priority order. (TODO:[HFT-245])
An order can only be rejected if the opposite side does not contain enough.


### Stop applications

(TODO:[HFT-53])
<!-- ~~Стоп-заявки срабатывают по достижении стоп-цены, которая может быть задана на освнове индикатора:
последней сделки, индекса маркировки контракта, индексе справедливой цены или индекса спот на кросс-пару.
Направление стоп-заявки определяет стоп-цена, и если она выше заданного индикатора,
то это заявка на покупку, если ниже - на продажу. 
После срабатывание тригера, стоп-заявка становится исполненной и попадает в список обычных заявок,
в качестве: лимитированной или рыночной.~~ -->


### Order Cancellation

If the order has not yet been executed, it can be canceled, or partially canceled, with only one order identifier. Cancellation of regular orders and stop orders occurs in different ways.

### Editing an Order

(TODO:[Under development])


### Order statuses

An order can be updated and its status changed from the moment it appears in the trading system (even without the participation of a client, for example, a large order is gradually filled with small transactions - its filled_quantity changes), which allows its `filled_quantity`) history to be traced in detail:

* `ORDERSTATUS_PENDINGNEW` - allocated orders that have passed risk control and taken into account in the initial guarantee collateral
* `ORDERSTATUS_NEW` - order registered in the match
* `ORDERSTATUS_FILLED` - order executed in full
* `ORDERSTATUS_PARTIAL` - the order is partially executed
* `ORDERSTATUS_PENDINGCANCEL` - application registered for cancellation
* `ORDERSTATUS_CANCELLED` - order canceled completely
* `ORDERSTATUS_PARTIALCANCELLED` - the order is partially executed, partially canceled
* `ORDERSTATUS_REJECTED` - application rejected for one of the reasons, and specified in `status_description`
* `ORDERSTATUS_REJECTEDBYMATCHER` - application rejected by the matcher
* `ORDERSTATUS_PENDINGREPLACE` - (TODO:[Under development])


### Order processing 

Orders with a status of having passed the risk control stages have the `ORDERSTATUS_PENDINGNEW` status and do not yet have a registration time, in this form they are sent to the matcher, where all orders received in one exchange tick are assigned the current central time. Cancellation requests are processed first (justification!).

The next step is to run the fair pricing algorithm in the matcher, which is based on the Price-Time algorithm.

For this, all market orders are considered as orders with the best price, which means they have the highest priority for execution. (In development - now first market, then limited)

The difference between the algorithm is that the distribution of the price premium within a tick, between cross orders, has the least square deviation, that is, for each case of cross orders, the fairest price for the two parties will be determined, and as a result, it excludes the  unacceptable case when the entire premium goes to one order.

After that, for post-orders, the auction price is found within the existing spread, at which all buy orders will be registered below this price and all sell orders - above this price. The auction price is chosen in such a way that the largest volume of bids can be registered.

[The example of matcher’s work](https://matcher-example.xquanto.com/#/exchange/MatcherExampleP)

Fair pricing algorithm http://todo


### Order Display

Orders are displayed in short form (level I): the bid price (the best buy price), the ask price (the best seller price) and the number of contracts at these prices.

And detailed (level II, order book): where all orders in an impersonal form are combined at all price levels.

Order Book (level III): with information about the account from which the order was sent. The order book is not available to anyone except the matcher himself who processes orders.




