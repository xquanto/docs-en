[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Liquidation

### Bundle leverage assessment

To evaluate the leverage, we will assume that the leverage is a quantitative coefficient reflecting all liabilities in relation to the current balance. Since risk control is carried out within the bundle, the first step is to assess the current balance of the bundle.

Current realized portfolio balance (current estimated balance, excluding unrealized profit):

```math
\tilde{V}^{portfolio} = B^{solid} + R^{portfolio} + S^{portfolio} - F^{portfolio} + G^{portfolio}
```

$`\tilde{V}^{portfolio}`$ - current realized portfolio balance   
$`B^{solid}`$ - the firm balance of the account  
$`R^{portfolio}`$ - realized profit on portfolio transactions   
$`S^{portfolio}`$ - portfolio swap   
$`F^{portfolio}`$ - commissions for portfolio transactions   
$`G^{portfolio}`$ - accumulated refinancing in a portfolio  

Free realized portfolio funds:

```math
\tilde{А}^{portfolio}_{usd} = \tilde{V}^{portfolio} I_{cur} - \check{T}^{portfolio}
```

$`\tilde{А}^{portfolio}_{usd}`$ - free realized portfolio funds in dollars  
$`I_{cur}`$ - the weighted index of the portfolio currency rate  
$`\check{T}^{portfolio}`$ - general cross-collateral (initial and maintenance) of the portfolio 

To assess the free realized bundle funds, it is assumed that they are distributed in proportion to the supporting cross-provision of all bundles:

```math
\tilde{А}^{bun}_{usd} = \tilde{А}^{portfolio}_{usd} \frac{\check{M}^{bun}}{\check{M}^{portfolio}}
```

$`\tilde{А}^{bun}_{usd}`$ - free implemented bundle tools  
$`\check{M}^{bun}`$ - cross-linking bundle support   
$`\check{M}^{portfolio}`$ - Supported portfolio cross-collateral  

Thus, the current estimated balance of the bundle is:

```math
V^{bun}_{usd} = \tilde{А}^{bun}_{usd} + \check{T}^{bun} + U^{bun} + Q^{bun}
```

$`V^{bun}_{usd}`$ - the estimated balance of the bundle     
$`U^{bun}`$ - unrealized profit in the bundle   
$`Q^{bun}`$ - not implemented bundle swap    

Liabilities for open items in the bundle, expressed as guaranteed supported collateral, taken without coverage:

```math
L^{bun} = M^{bun} / b_m
```

$`L^{bun}`$ - commitments for open positions in the bundle    
$`M^{bun}`$ - support margin    
$`b_m`$ - base ratio of the backing collateral
 
The bundle leverage is calculated as follows:

```math
l = \frac{L^{bun}}{V^{bun}_{usd}}
```

$`l`$ - bundle shoulder


### Evaluation of liquidation levels

Upon bundle liquidation, a liquidation commission is charged, which is 0.4 USD for each futures contract and 0.6 USD for an option. The total liquidation commission for the bundle will be considered $`F^{bun}_{liq}`$.

```math
F^{bun}_{liq} = b^{fut}_{liq} \sum |q^{fut}| + b^{opt}_{liq} \sum |q^{opt}|
```

$`F^{bun}_{liq}`$ - bundle liquidation appraisal commission  
$`q^{fut}`$ - number of futures in position   
$`q^{opt}`$ - the number of options in the position  
$`b^{fut}_{liq}`$ - liquidation commission for one futures contract  
$`b^{opt}_{liq}`$ - liquidation commission for one option contract  

The main criterion for the absence of the need for liquidation will be the consideration of the sufficiency of the estimated balance of the bundle, at the current price of the underlying asset $`s_0`$, which is the initial condition:

```math
V(s_0) = V^{bun}_{usd} - F^{bun}_{liq} > 0
```

!!! However, in general terms, the bundle assessment function will be found as: $`V(\Delta s)`$, depending on the price of the underlying asset, from the evaluation of the bundle, as decay of the portfolio for the first greek of the spot price:

<!-- А в общем виде, найдём функцию оценки бандла $`V(\Delta s)`$, в зависимости от цены базовго актива,
из оценки бандла, как [разложение портфеля по первым грекам спот-цены](greek.md#разложение-портфеля-на-грекки): -->

```math
dP^{bun} \approx \mathrm{delta}\enspace dp + \frac{1}{2} \mathrm{gamma}\enspace d^2p
```

$`dP^{bun}`$ - model portfolio from one bundle   
$`\mathrm{delta}`$ - bundle delta  
$`\mathrm{gamma}`$ - bundle gamma    
$`dp`$ - percentage increment of $`s`$  

Or with initial conditions:

```math
V(s_0 + \Delta s)-V(s_0) = \frac{dP^{bun}}{dp}
```

Expanded:

```math
V(s_0 + \Delta s) = \mathrm{delta}\enspace \Delta p + \frac{1}{2} \mathrm{gamma}\enspace (\Delta p)^2 + V^{bun}_{usd} - F^{bun}_{liq}
```

where the percentage increment of the spot price is used:

```math
\Delta p = 100 \cdot \frac{s_0 + \Delta s}{s_0}
```

To predict liquidation levels: it is necessary to find at what percentage increment the bundle evaluation function will become negative, and find the intersection points of the parabola $`ax^2 + bx + c`$ with the horizontal axis, where:

```math
\begin{aligned}
a 	&= \frac{1}{2} \mathrm{gamma}  \\
b 	&= \mathrm{delta} \\
c 	&= V^{bun}_{usd} - F^{bun}_{liq} \\
D 	&= \sqrt{b^2 - 4ac}
\end{aligned}
```

$`D`$ - quadratic discriminant.

The following cases of solving this inequality are possible:

1. $`c < 0`$: immediate liquidation
2. $`c \geqslant 0, a = 0, b = 0`$: $`p_- = -99.(9)`$ и $`p_+ = +\infty`$
3. $`c \geqslant 0, a = 0, b > 0`$: $`p_- = -c/b`$ и $`p_+ = +\infty`$
4. $`c \geqslant 0, a = 0, b < 0`$: $`p_- = -99.(9)`$ и $`p_+ = -c/b`$
5. $`c \geqslant 0, a > 0, D < 0`$: $`p_- = -99.(9)`$ и $`p_+ = +\infty`$
6. $`c \geqslant 0, a > 0, D \geqslant 0, b > 0`$: $`p_- = \frac{-b + \sqrt d} {4a}`$ и $`p_+ = +\infty`$
7. $`c \geqslant 0, a > 0, D \geqslant 0, b < 0`$: $`p_- = -99.(9)`$ и $`p_+ = -\frac{b + \sqrt d} {4a}`$
8. $`c \geqslant 0, a < 0, D \geqslant 0, b > 0`$: $`p_- = \frac{-b + \sqrt d} {4a}`$ и $`p_+ = -\frac{b + \sqrt d} {4a}`$
9. $`c \geqslant 0, a < 0, D \geqslant 0, b < 0`$: $`p_- = -\frac{b + \sqrt d} {4a}`$ и $`p_+ = \frac{-b + \sqrt d} {4a}`$

$`p_-`$ - percentage difference from the current price down   
$`p_+`$ - percentage difference from the current price up 

For options, the dependence on large parameters introduces an additional error in the forecast of liquidation levels, but with a decrease in the level of the estimated balance of the bundle, the accuracy increases, and follows the futures model, nevertheless, if there are more options than futures in the portfolio, the difference up and down is multiplied by the correction optional coefficient 0.8.

Convert the percentage increments back to linear ones and find the bundle liquidation levels at spot prices:

```math
s_- = s_0 (1 + 0.01 \cdot p_-) \\
s_+ = s_0 (1 + 0.01 \cdot p_+)
```

$`s_-`$ - lower level of the spot price of bundle liquidation   
$`s_+`$ - upper level of the spot price of bundle liquidation   


### Terms and conditions of bundle liquidation

Liquidation levels are strictly advisory in nature, and are not definitive criteria. When the spot price exits the range of liquidation levels, there is a warning to check the ways to avoid it. Liquidation will not be carried out until methods to prevent it are used, in the order described below:

1. The bounty is reduced to the lowest possible value, greater than 1.
2. The process of cancellation of orders is started until there are no active bids

After each method, it is checked if the method has yielded a result - liquidation will not take place at this time. For liquidation to ensue, it is necessary to re-confirm that the estimated balance is insufficient at that moment.



