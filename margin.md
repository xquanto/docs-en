[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Methodology for calculating Margin

### Perpetual, futures and options contracts

For any separate contract, within the framework of one position, there are two types of collateral: initial - when making an order, and supporting - after the transaction is completed in proportion to the ordered position. Both of them depend on the underlying initial collateral and the underlying backing collateral, respectively, set as contract-specific constants (typically USD 2 and USD 1).

For futures and perpetual contracts (main and composite), the values ​​are determined by the formula:

```math
\begin{aligned}
I^c 	&= b_i \\
M^c 	&= b_m \max(1, \mathit{delta})
\end{aligned}
```

and for an option:

```math
\begin{aligned}
I^c 	&= b_i max(1, p_o/100) \\
M^c 	&= b_m max(1, \mathit{delta})
\end{aligned}
```

$`I^c`$ - initial guaranteed collateral of the contract   
$`M^c`$ - supporting guaranteed contract collateral   
$`b_i`$ - basic initial collateral   
$`b_m`$ - basic backing guaranteed collateral   
$`delta`$ - contract delta   
$`p_o`$ - option marking price, for a market order, or the price of a limit order 


### Position

Knowing the average entry price, it is necessary to enter an adjustment coefficient for the quanto-futures, proportional to the change in financial result, to adjust the supporting guaranteed collateral:

```math
k^f = \begin{cases}
p_a/p_m		&\text{, для фьючерса}\\
1 			&\text{, для опциона}
\end{cases}
```

$`k^f`$ - correction factor for quanto-futures   
$`p_a`$ - average position entry price  
$`p_m`$ - futures marking price  


Then the assessment of the supporting collateral is made, which, for convenience, is divided into long and short (one of them is equal to 0), it is also necessary to calculate the coverage that will later be needed to assess the cross-margin.


```math
\begin{aligned}
M^{pos}_l 	&= q_l k^f M^c \\
M^{pos}_s 	&= q_s k^f M^c \\
M^{pos} 	&= M^{pos}_s + M^{pos}_l \\
C^{pos}_l 	&= 0.8 M^{pos}_s \\
C^{pos}_s 	&= 0.8 M^{pos}_l
\end{aligned}
```

$`q_l`$ - number of purchased contracts   
$`q_s`$ - number of sold contracts   
$`M^{pos}_l`$ - Long Position supporting guaranteed Collateral  
$`M^{pos}_s`$ - Short Position supporting guaranteed Collateral  
$`M^{pos}`$ - Position Supporting Guarantee    
$`C^{pos}_l`$ - coverage for a long position    
$`C^{pos}_s`$ - coverage for a short position    

The coverage ratio is fixed at 0.8.

For a directed initial position guarantee:

```math
\begin{aligned}
I^{pos}_l 	&= q_{buy} I^c \\
I^{pos}_s 	&= q_{sell} I^c \\
\end{aligned}
```

where

$`I^{pos}_l`$ - initial grant collateral for long positions    
$`I^{pos}_s`$ - initial grant collateral for short positions    
$`q_{buy}`$ - number of purchased contracts   
$`q_{sell}`$ - number of sold contracts   


### Bundle

Coverage from other positions appears in the bundle, where the risks are combined by one underlying asset, therefore, the initial supporting collateral for the bundle is calculated, as well as cross-collateral for the bundle:

```math
\begin{aligned}
C^{bun}_l 			&= \sum C^{pos}_l \\
C^{bun}_s 			&= \sum C^{pos}_s \\
M^{bun}_l 			&= \sum M^{pos}_l \\
M^{bun}_s 			&= \sum M^{pos}_s \\
M^{bun} 			&= M^{bun}_l + M^{bun}_s \\
\check{M}^{bun} 	&= \max(M^{bun}_l - C^{bun}_l, M^{bun}_s - C^{bun}_s)
\end{aligned}
```

$`C^{bun}_l`$ - coverage for long positions of the bundle    
$`C^{bun}_s`$ - coverage for short positions of the bundle   
$`M^{bun}_l`$ - supporting guaranteed collateral for long bundle positions  
$`M^{bun}_s`$ - supporting guaranteed collateral for short bundle positions    
$`M^{bun}`$ - supporting guaranteed bundle collateral  
$`\check{M}^{bun}`$ - supporting cross-collateral of the bundle  

Initial guaranteed collateral:

```math
\begin{aligned}
I^{bun}_l 		&= \sum I^{pos}_l \\
I^{bun}_s 		&= \sum I^{pos}_s \\
I^{bun} 		&= \max(I^{bun}_l, I^{bun}_s) \\
\check{I}^{bun} &= \max(I^{bun}_l - M^{bun}_s, I^{bun}_s - M^{bun}_l)
\end{aligned}
```

$`I^{bun}_l`$ - initial collateral for long bundle positions  
$`I^{bun}_s`$ - initial collateral for short bundle positions   
$`I^{bun}`$ - guaranteed initial collateral of the bundle   
$`\check{I}^{bun}`$ - initial cross-collateral of the bundle  

To assess the risks, only the cross-collateral of the bundle is taken into account, the guaranteed collateral is for informational purposes only.

General guaranteed and cross-collateral of the bundle:

```math
\begin{aligned}
T^{bun} 		&= M^{bun} + I^{bun} \\
\check{T}^{bun}	&= \check{M}^{bun} + \check{I}^{bun}
\end{aligned}
```

$`T^{bun}`$ - general guaranteed collateral of the bundle  
$`\check{T}^{bun}`$ - general bundle cross-collateral  


### Portfolio

For a collateral portfolio, it is calculated as a direct sum for all bundles: 

```math
\begin{aligned}
I^{portfolio} 			&= \sum I^{bun} \\
\check{I}^{portfolio} 	&= \sum \check{I}^{bun} \\
M^{portfolio} 			&= \sum M^{bun} \\
\check{M}^{portfolio} 	&= \sum \check{M}^{bun} \\
\end{aligned}
```

$`I^{portfolio}`$ - initial portfolio collateral  
$`\check{I}^{portfolio}`$ - initial portfolio cross-collatera   
$`M^{portfolio}`$ - guaranteed supporting collateral of the portfolio  
$`\check{M}^{portfolio}`$ - supporting cross-collateral of the portfolio  

General and cross-collateral margin of the portfolio:

```math
\begin{aligned}
T^{portfolio} 			&= M^{portfolio} + I^{portfolio} \\
\check{T}^{portfolio}	&= \check{M}^{portfolio} + \check{I}^{portfolio}
\end{aligned}
```

$`T^{portfolio}`$ - total portfolio guaranteed coverage  
$`\check{T}^{portfolio}`$ - total portfolio cross-collateral  


### Account Bounty

For an open position on one contract there is a limit on the number of contracts equal to 10,000. 
The bounty $`b`$ specified by the client, in the range from 1 to 10 inclusively, allows this limitation to be proportionally increased, while all parameters of the initial and supporting collateral will be increased proportionally to $`b^2`$.
