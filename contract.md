[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Contracts

### Naming

Futures perpetual contracts:  
`BTCUSD-ZZ` - perpetual futures for the `BTCUSD`

Futures term contracts (cross pair, letter of the month, digit of the year):  
`BTCUSD-H1` - `BTCUSD`, March, 2021, for futures always the  $3^{rd}$ Friday

|Month		|Code 	|
|-----------|-------|
|January		|F 		|
|February	|G 		|
|March		|H 		|
|April		|J 		|
|May		|K 		|
|June		|M 		|
|July		|N 		|
|August		|Q 		|
|September	|U 		|
|October	|V 		|
|November		|X 		|
|December	|Z 		|

Option contracts (cross pair, letter of month and option type, digit of the year, strike of the first 3 significant digits, week code):
`BTCUSD-150C1B` - `BTCUSD`, March call, 2021 with a strike of 15,000, $2^{nd}$ Thursday

|Month		|Call code		|Put code 		|
|-----------|---------------|---------------|
|January	|A				|M 				|
|February	|B				|N 				|
|March		|C				|O 				|
|April		|D				|P 				|
|May		|E				|Q 				|
|June		|F				|R 				|
|July		|G				|S 				|
|August		|H				|T 				|
|September	|I				|U 				|
|October	|J				|V 				|
|November	|K				|W 				|
|December	|L				|X 				|

|Week of a month	|Code 	|
|-------------------|-------|
|First Thursday		|A 		|
|Second Thursday	|B 		|
|Third Thursday		| 		|
|Fourth Thursday	|D 		|
|Fifth Thursday		|E 		|

$`k`$ option strike is defined as follows:

```math
k = x/1000*10^{\lceil{\log_{10}X}\rceil}
```

$`x`$ - three-digit strike value  
$`X`$ - the maximum base strike upon option contract registration    
$`\log_{10}`$ - decimal logarithm  


### Inverse Quanto-Futures

Quanto futures are futures in which the value of the underlying asset is calculated in a local currency, and the financial result is calculated in a foreign currency. A special case are inverse quanto-futures, when the asset is a foreign currency (the currency of the financial result) (for example, buying BTC against the dollar, you make a profit in BTC).


### Fixed -Term Major Futures Contracts

A fixed-term futures contract on the main cross-pair (foreign currency/dollar) is a calculated inverse quanto-futures on the weighted index of the main cross-pair, where the dollar is the local currency, and the financial result will be in a foreign currency.

Each contract, regardless of the currency and the current price of the underlying asset, contains liabilities equivalent to USD 100.

The expiration is determined by the name of the contract and can be in one of the quarterly months (March, June, September, December), on the third Friday (the last nanosecond).

At the time of expiration, a calculation will be made: unrealized profit will be converted into a financial result at a fixed, and the same for all clients, calculated spot price.

More details about the guarantee provision and examples of calculating the financial result for the main contracts.

More details about [the guarantee provision](margin.md) and examples of calculatin [the financial result for the main contracts.](exec.md#inverse-quanto-futures-on-main-cross-pair)


### Fixed-Term Bonded Futures Contracts

Unlike main contracts, in bonded contracts, the underlying asset is a bonded cross pair, `XXXYYY` in which there is no dollar. Such a contract consists of one long contract `XXXUSD-MY`and one short `YYYUSD-MY` on the same date.

A client, when making transactions on composite futures, can be guided by the properties of the model:

* If the price has not changed, then the profit is 0.
* If the price is up, then the long position is in profit (short is at a loss).
* If the price falls, then the short position is in profit (long is at a loss).

However, to calculate the exact financial result, the price of entry and exit of a futures contract alone is not enough, and the financial result is calculated separately for the long and short components.

Examples of calculating [the financial result for composite contracts.](exec.md#inverse-quanto-composite-cross-pair-futures)


### Perpetual futures contracts

Perpetual futures can be for major and composite crosses. At the core of such a perpetual contract in every 8 hour interval is the $`c_n`$ model delivery contract.
At the time of its expiration, another $`c_{n+1}`$ model contract is delivered for the same cross-pair, with an expiration date in the next 8 hours, and compensation by the expressed refinancing rate, adjusted to an 8-hour interval.

How [the refinancing rate](index.md) is calculated.


### Option contracts

The underlying asset of an option contract is one futures contract with the nearest quarterly expiration (all futures are quarterly) per main cross pair. Futures delivery is carried out for all options in the money at the time of expiration, by automatic claim of the right, at the strike price and the direction corresponding to the type of option for the buyer (call - long position, put - short position),and the opposite direction of the option type for the seller of the option..

The financial result for option contracts is calculated in the same way as for a linear financial instrument.

[Learn more about calculating profit on an options contract.](exec.md#vanilla-options-contract-on-inverse-quanto-futures-on-the-main-cross-pair)