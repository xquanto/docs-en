[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]


## Account – trading sub-account

### Available currencies

Trading accounts are available in the following currencies:

* USD - USD
* BTC - Bitcoin
* ETH - Etherium
* LTC - Litecoin
* XRP - Ripple


### Transfer to a wallet

Transfer from a sub-account to a wallet is fee-less and is possible if the wallet currency is the same as the sub-account currency. The amount available for withdrawal is defined by [the available funds](available-funds.md#balance-and-available-funds).
[The following rules](wallet.md#transfer-to-a-trading-sub-account) apply for transfers from the wallet to the trading sub-wallet.


### Hard balance of a trading sub-account

A trading sub-account is comprised of the following balances:

* Transfers to a wallet
* Transfers from a wallet
* Trade balance - financial result for closed portfolios 
* Liquidation compensation 

Validation of each type of balance is carried out by the sum of transactions of the corresponding type.

```math
B^{solid} = N^{a \larr w} - N^{a \rarr w} + N^{trade} + N^{liq}
```

$`B^{solid}`$ - hard balance of an account   
$`N^{a \larr w}`$ - all amounts added to an account from a walle   
$`N^{a \rarr w}`$ - all amounts withdrawn from an account to a wallet   
$`N^{trade}`$ - financial result of transactions in all closed portfolios  
$`N^{liq}`$ - compensation for a negative balance possibly upon liquidation   

[Financial result for closed portfolios.](account-current.md#portfolio-financial-result)


### General currency - USD

Margin and unrealized profit shall be calculated in USD, and only after fixing, shall be converted into the account’s currency at the rate of the weighted index. The compensation or premium for funding in the account currency (in exchange for USD) shall be calculated as a mechanism with an embedded swap.
	

### Swap

Profit upon closing a position or partial closure of a position (as of the transaction) shall be determined in USD, and then converted at the current rate of the weighted index into the account’s currency. From the moment of opening a position to the moment of closing, due to the difference in rates for currencies between a local currency and an account’s currency, a currency swap shall be formed (without delivery) and its rates will be fixed at the time of its closure (payer swaption on NDS), in accordance with overnight indices for both currencies.

Read more about [swap calculation](exec.md#fixing-a-position) and 
[overnight currency rates](index.md#overnight-currency-rate-n).