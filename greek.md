[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Greeks

### Portfolio Decay determined by greeks 

The Greeks allow for a more accurate risk assessment, and to inform a client of the detailed characteristics of their synthetic position (bundle), depending on the dynamics. 

Here and hereinafter we will use the basic units of measurement $`\Delta`$ for forecasting of portfolio dynamics:
* $`\Delta s`$ - 1% positive movement of the spot price 
* $`\Delta v`$ - 1 percentage point of the expected volatility rate positive movement 
* $`\Delta t`$ - decrease by 1 calendar day
* $`\Delta r`$ - 1 percentage point of forward rates difference positive movement

where:

$`s`$ - underlying asset price (spot)  
$`v`$ - expected volatility  
$`t`$ - time to expiration   
$`r`$ - difference in rates for currencies $`r_2-r_1`$

In this case, the first-order Greeks take the following form and affect the unrealized profit of the portfolio:
* `delta` - if the spot price changes by 1%
* `vega` - if the expected volatility changes by 1 percentage point
* `theta` - if 1 calendar day has passed
* `rho` - if the difference in forward rates changes by 1 percentage point

To complete the picture, we need an approximation by the Taylor formula with partial derivatives:

```math
dP \approx \mathrm{delta}\enspace ds + \mathrm{vega}\enspace dv + \mathrm{theta}\enspace dt + \mathrm{rho}\enspace dr + \\
\frac{1}{2} \mathrm{gamma}\enspace d^2s + \frac{1}{2} \mathrm{vomma}\enspace d^2v + \mathrm{vanna}\enspace ds dv + \mathrm{charm}\enspace ds dt + \mathrm{veta}\enspace dv dt + \mathrm{vera}\enspace dv dr
```

The second-order Greeks help to more accurately assess portfolio changes or predict first-order Greeks in the same units:

$`dP`$ - portfolio changes  
`gamma` - the second derivative for $`\partial^2 s`$  
`vomma` - the second derivative for $`\partial^2 v`$  
`vanna` - the first partial derivative for $`\partial s \partial v`$  
`charm` - the first partial derivative for $`\partial s \partial t`$  
`veta` - the first partial derivative for $`\partial v \partial t`$  
`vera` - the first partial derivative for $`\partial v \partial r`$  

This is not a complete list of resolutions that can be used, but it gives you the necessary level of accuracy for decision-making.


### Inverse quanto-futures <a name="greek-future"></a>

Dependence of the value of futures on the underlying asset:

```math
F^{fut}(s,r,t) = s \mathrm{e}^{rt}
```

$`F^{fut}`$ - futures price  
$`r`$ - difference in rates for currencies $`r_2-r_1`$  
$`t`$ - time to futures expiration  

Price function for inverse quanto-futures or financial result calculating:

```math
V_a^{fut} = q\frac{(s - a F^{fut}(s,r,t))} {s}
```

or 

```math
V_a^{fut} = q\frac{(s - a \mathrm{e}^{-rt})} {s}
```

$`V_a^{fut}`$ - inverse quantum futures price function  
$`a`$ - average price of a financial instrument position   
$`q`$ - number of contracts with a position direction sign 

First-order Greeks:

```math
\begin{aligned}
\mathrm{delta} 	&= 1/100 \mathrm{e}^{-rt} \\
\mathrm{vega} 	&= 0 \\
\mathrm{theta} 	&= -1/365 r \mathrm{e}^{-rt} \\
\mathrm{rho} 	&= 1/100 t \mathrm{e}^{-rt}
\end{aligned}
```


### Options <a name="greek-option"></a>

Dependence of the cost of a vanilla option on an inverse quanto-futures contract on the underlying asset:

```math
\begin{aligned}
F^{call}(s,k,v,t,u)	&= \dfrac{\mathrm{e}^{-r(t+u)}\left(s\mathrm{e}^{rt}\left(E_1+1\right)-k\left(E_2+1\right)\right)}{2s}  \\
F^{put}(s,k,v,t,u) 	&= \dfrac{\mathrm{e}^{-r(t+u)}\left(s\mathrm{e}^{rt}\left(E_1-1\right)-k\left(E_2-1\right)\right)}{2s}
\end{aligned}
```

$`F^{call}`$ - call option cost  
$`F^{put}`$ - put option cost  
$`s`$ - spot price (underlying asset of a quantum futures)  
$`k`$ - option strike  
$`r`$ - difference in rates for currencies $`r_2-r_1`$  
$`v`$ - volatility  
$`t`$ - time to option expiration   
$`u`$ - time from option expiration to futures expiration   

And instead of a cumulative function, the cost is expressed directly through the error integral:

```math
\begin{aligned}
E_1		&= \operatorname{erf}\left(\frac{\ln\left(\frac{s\mathrm{e}^{rt}}{k}\right)+tv^2}{v\sqrt{2t}}\right) \\
E_2 	&= \operatorname{erf}\left(\frac{\ln\left(\frac{s\mathrm{e}^{rt}}{k}\right)-tv^2}{v\sqrt{2t}}\right)
\end{aligned}
```

price function for a vanilla option or financial result calculation:

```math
\begin{aligned}
V_a^{call}	&= q(F^{call} - a)  \\
V_a^{put} 	&= q(F^{put} - a)
\end{aligned}
```

$`V_a^{call}`$ - price function for a call option on inverse quantum futures   
$`V_a^{put}`$ - price function for an inverse quantum futures put option   
$`a`$ - средняя цена позиции финансового инструемента   
$`q`$ - number of contracts with a position direction sign  


The following substitute will be used to abbreviate the greeks:

```math
Q = \sqrt{\dfrac{k} {2\pi s}} \mathrm{e}^{-\frac{\ln^2\left(\frac{s\mathrm{e}^{rt}}{k}\right)}{2tv^2}-\frac{tv^2}{8}-ru-\frac{rt}{2}}
```

First-order Greeks:

```math
\begin{aligned}
\mathrm{delta^{call}} 	&= 1/100 \dfrac{k\mathrm{e}^{-r(u+t)}\left(E_2+1\right)}{s} \\
\mathrm{delta^{put}} 	&= 1/100 \dfrac{k\mathrm{e}^{-r(u+t)}\left(E_2-1\right)}{s}
\end{aligned}
```

```math
\begin{aligned}
\mathrm{vega^{call}} 	&= 1/100 \sqrt{t} Q \\
\mathrm{vega^{put}} 	&= 1/100 \sqrt{t} Q
\end{aligned}
```

```math
\begin{aligned}
\mathrm{theta^{call}} 	&= 1/360 \dfrac{kr\mathrm{e}^{-r(u+t)}(E_2+1)}{2s} + \dfrac{v}{2\sqrt{t}}Q \\
\mathrm{theta^{put}} 	&= 1/360 \dfrac{kr\mathrm{e}^{-r(u+t)}(E_2-1)}{2s} + \dfrac{v}{2\sqrt{t}}Q
\end{aligned}
```

```math
\begin{aligned}
\mathrm{rho^{call}} 	&= 1/100 \dfrac{\mathrm{e}^{-r(u+t)}}{2} \left( -u\mathrm{e}^{rt} (E_1 + 1) + \frac{k}{s} (u+t) (E_2 + 1) \right) \\
\mathrm{rho^{put}} 		&= 1/100 \dfrac{\mathrm{e}^{-r(u+t)}}{2} \left( -u\mathrm{e}^{rt} (E_1 - 1) + \frac{k}{s} (u+t) (E_2 - 1) \right)
\end{aligned}
```
