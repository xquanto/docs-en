[Client Documentation](https://gitlab.com/xquanto/docs-en) 

[[_TOC_]]

## Balance and available funds

### Realized profit

The financial result on fixing deals (transactions that lead to a decrease in the position)  accumulates, until the position is closed, until the bundle is closed, or until the portfolio is closed, respectively, and consists of the following components.

Realized profit:

$`R^{pos}`$ - realized profit on trades in a position  
$`R^{bun}`$ - realized profit on transactions in a bundle   
$`R^{portfolio}`$ - realized profit on portfolio transactions   

Swap:

$`S^{pos}`$ - swap for transactions in a position  
$`S^{bun}`$ - swap for transactions in a bundle  
$`S^{portfolio}`$ - portfolio swap  

Commissions:

$`F^{pos}`$ - commissions for transactions in a position   
$`F^{bun}`$ - commissions for transactions in a bundle  
$`F^{portfolio}`$ - commissions for portfolio transactions   

Refinancing:

$`G^{pos}`$ - accumulated refinancing in a position   
$`G^{bun}`$ - Accumulated Refinancing in a Bundle  
$`G^{portfolio}`$ - accumulated refinancing in a portfolio   

Read more about the formulation of [commission](exec.md#trading-commission), [realized profit](exec.md#fixing-a-position), swap and [refinancing](expiration.md#refinancing-perpetual-contracts)


### Unrealized profit

Unrealized profit is assessed periodically in the same way as profit fixation, but as a fixing transaction, the entire volume is considered by the value of the marking index, without converting the result into the account currency.

$`U^{pos}`$ - unrealized profit on trades in a position   
$`U^{bun}`$ - unrealized profit on transactions in the bundle   
$`U^{portfolio}`$ - unrealized profit on portfolio transactions
 

Unrealized swap is assessed periodically in the same way as profit-taking, for the entire volume at current overnight rates.


$`Q^{pos}`$ - unrealized position swap  
$`Q^{bun}`$ - unrealized bundle swap  
$`Q^{portfolio}`$ - unrealized portfolio  

Read more about the marking index [for futures](index.md#labeling-index-for-futures-m),
[for perpetual contracts ](index.md#perpetual-contract-marking-index-m) and [options](index.md#option-labeling-index-m), and also
[the overnight rate for currency](index.md#overnight-currency-rate-n)


### Balance and funds available

The current estimated balance of the portfolio:

```math
V^{portfolio} = B^{solid} + R^{portfolio} + S^{portfolio} - F^{portfolio} + G^{portfolio} + (U^{portfolio} + Q^{portfolio})\cdot I_{cur}
```

$`V^{portfolio}`$ - the current estimated balance of the portfolio  
$`B^{solid}`$ - the hard balance of the account   
$`I_{cur}`$ - weighted index of account currency  

And free funds:

```math
А^{portfolio} = V^{portfolio} - \frac{\check{T}^{portfolio}} {I_{cur}}
```

$`А^{portfolio}`$ - free portfolio funds  
$`\check{T}^{portfolio}`$ - general cross-collateral (initial and maintenance) of the portfolio   
$`I_{cur}`$ - the value of the account currency as a weighted index value   

Read more about [weighted index for general cross pair](index.md#weighted-index-for-the-general-cross-pair-)

