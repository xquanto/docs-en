[Client Documentation](https://gitlab.com/xquanto/docs-en)

[[_TOC_]]

## Currency account (wallet)

After the registration is completed, the following currency wallets are reserved for the client:

* USD - USD
* EUR - Euro
* BTC - Bitcoin
* ETH - Etherium
* LTC - Litecoin
* XRP - Ripple
* USDT - Tether

For fiat currencies (USD, EUR) unique account details are created and for cryptocurrencies (BTC, ETH, LTC, XRP, USDT) unique [HD Wallets](https://en.bitcoin.it/wiki/Deterministic_wallet) are created.


### Safety

The currency account displays funds that are not involved in trading and are ready for withdrawal or conversion. At the same time, the funds themselves, expressed in cryptocurrency, are stored in cold storage, which is accessed through the Treasury department. Our partner bank is responsible for the safety of fiat funds.

Withdrawal of funds to an external wallet is carried out at the request of the client, if the balance of all the client’s wallets is successfully validated; the request is sent to the finance department.

If the requested amount, in the specified currency, is sufficient in the finance department the request is processed automatically. Otherwise, it will be forwarded to the Treasury, which will replenish the accounts of the Finance Department, and the client's request in this case will be processed within the next 24 hours.

In the case of fiat currency, an order is sent to the partner bank to make a bank transfer.


## Wallet balance

The balance of an individual wallet is realized as the sum of the following balances:

```math
B^{wallet} = N^{deposit} - N^{withdraw} + N^{w \larr w_{cur}} - N^{w \rarr w_{cur}} + N^{w \larr a} - N^{w \rarr a} + N^{referral}
```

$`B^{wallet}`$ - available wallet balance  
$`N^{deposit}`$ - the sum of all deposits from external wallets   
$`N^{withdraw}`$ - сthe sum of all withdrawals to external wallets   
$`N^{w \larr w_{cur}}`$ - the sum of all incoming conversions from wallets in another currency  
$`N^{w \rarr w_{cur}}`$ - the sum of all outgoing conversions to wallets in another currency  
$`N^{w \larr a}`$ - the sum of all replenishments of the wallet from the account  
$`N^{w \rarr a}`$ - the sum of all withdrawals from the wallet to the account  
$`N^{referral}`$ - the sum of all referral charges  

Validation of each type of balance is carried out by the sum of transactions of the corresponding type. The sum of all balances reflects the balance of the wallet.


### Account Replenishment

Account replenishment is carried out without restriction. Fiat funds will be credited to a foreign currency account upon receipt of a bank deposit, subject to the correct indication of the purpose of the payment (otherwise, the funds will be returned after deducting the bank transfer fee).

The cryptocurrency will be added upon reaching a reliable number of confirmations in the blockchain network, which corresponds to:

* BTC - 3
* ETH - 12
* LTC - 4
* XRP - NA
* USDT - 12


### Withdrawal

Withdrawal (debiting) of funds in fiat currency (todo: specify) is made to the same bank account from which the deposits were made.

The amount of withdrawal is subject to daily restrictions in the equivalent of USD:

1. Without 2FA, without KYC: 1000 USD
2. 2FA only: 2000 USD
3. KYC only: 5000 USD
4. 2FA and KYC: unlimited

The withdrawal limit in case #4 is determined as the sum of unused capacities for the last 24 hours, in the order of their formation. Power is fixed as 1/12 of the current amount, but not less than 1000 USD:

|Time, hour |Amount 		|Power	|Power after |Limit 		|Withdrawn 		|
|-----------|----------:|----------:|--------------:|----------:|----------:|
|1(-35)		|240 000	|20 000		|0				|20	000		|0			|
|...		|240 000	|20 000		|→0				|...		|0			|
|10(-26)	|240 000	|20 000		|20 000			|200 000	|0			|
|11(-25)	|240 000	|20 000		|20 000			|220 000	|←180 000	|
|12(-24)	|60 000		|5 000		|5 000			|45	000		|0			|
|...		|60	000		|5 000		|5 000			|...		|0			|
|34(-2)		|60 000		|5 000		|5 000			|150 000	|0			|
|35(-1)		|60 000		|5 000		|5 000			|135 000	|0			|
|36(0)		|60 000		|5 000		|5 000			|120 000	|0			|
|...		|60 000		|5 000		|5 000			|120 000	|0			|

*Attention! A trivial example: in #4 case, for large amounts, to withdraw the entire amount at once, if half of the amount has been credited on the account for the last 24 hours, or the entire amount has been in the last 12 hours* 


### Conversion

Conversion is carried out at the rate of the weighted cross-pair index, minus the commission. The commission depends on the conversion direction and can vary from 0.5% to 1.5%. The conversion limit is equivalent to USD 300K in a 30-minute period.

The weighted index [for the general](index.md#weighted-index-for-the-general-cross-pair-i) and 
[composite](index.md#weighted-index-for-the-composite-cross-pair-i) cross pairs.


### Transfer to a trading sub-account

Transfer from a wallet to a trading sub-account is both  fee-less and limitless and is possible if the wallet currency is the same as the sub-account currency.

[The following rules](account.md#transfer-to-a-wallet) apply for transfer from the trading sub-account to the wallet.


### Referral accruals

Referral accruals shall be credited to the USD-wallet. 
Read more about [the referral program.](referral.md#accrual-of-bonuses)